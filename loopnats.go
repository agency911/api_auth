package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"time"
)

// ConnectLoopNats - loop connection for NATS server
func ConnectLoopNats(timeout time.Duration, user, password, url string) (*nats.Conn, error) {
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()

	timeoutExceeded := time.After(timeout)
	for {
		select {
		case <-timeoutExceeded:
			return nil, fmt.Errorf("Nats connection failed after %s timeout", timeout)

		case <-ticker.C:

			nc, err := nats.Connect(url,
				nats.UserInfo(user, password),
				nats.ReconnectBufSize(5*1024*1024),
				nats.ReconnectWait(10*time.Second))
			if err == nil {
				return nc, nil
			}


		}
	}
}
