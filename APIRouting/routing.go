package APIRouting

import (
	"bitbucket.org/agency911/Api_Auth/logger"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type Config struct {
	log *logger.Logger
}

type Ok struct {
	Meta    *interface{} `json:"meta"`
	Success bool         `json:"success"`
	Message string       `json:"message"`
	Error   Error        `json:"error,omitempty"`
}

type Error struct {
	Err     *interface{} `json:"error"`
	Message string       `json:"message"`
}

func WrongParam() Ok {
	return Ok{Success: false, Message: "Wrong param"}
}

func WrongParamAnswer(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(WrongParam())
}

func Init() *httprouter.Router {
	router := httprouter.New()
	router.GET("/", Index)
	router.GET("/hello/:name", testMD(Hello))
	return router
}

func set(w *http.ResponseWriter) {
	(*w).Header().Set("Content-Type", "application/json")
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	set(&w)
	data := Ok{}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)

}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	set(&w)
	data := Ok{}
	data.Message = ps.ByName("name")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(data)
}

// Pure "github.com/julienschmidt/httprouter" middleware
func testMD(next httprouter.Handle) httprouter.Handle {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if ps.ByName("name") != "test" {
			WrongParamAnswer(w)
			return
		}
		next(w, r, ps)
	}
}
