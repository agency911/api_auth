package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
)

// NatLog
type NatLog struct {
	nats *nats.Conn
}

func (nc *NatLog) Write(p []byte) (n int, err error) {

	msg := &nats.Msg{
		Subject: "_LOG",
		Data:    p,
	}

	if nc.nats.PublishMsg(msg); err != nil {
		fmt.Println(p)
		return
	}

	return len(p), nil
}
