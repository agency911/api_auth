-- +migrate Up

CREATE TABLE revision(
    id SERIAL PRIMARY KEY,
    revision text NOT NULL DEFAULT ``,
);

-- +migrate Down
DROP TABLE revision;