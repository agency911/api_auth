package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"bitbucket.org/agency911/Api_Auth/logger"
	"github.com/nats-io/nats.go"
	"github.com/rs/zerolog"
	"golang.org/x/crypto/ssh/terminal"
)

func initial() *Config {
	var err error
	var file io.WriteCloser
	var log logger.Logger
	env := Env{}

	env.myConf, err = initConfig()
	if err != nil {
		panic(err)

	}

	var nc *nats.Conn
	if terminal.IsTerminal(int(os.Stdout.Fd())) || env.myConf.Logparam.Mode != "file" {
		// if env.myConf.Logparam.Mode != "file" {

		if env.myConf.Logparam.Mode != "NATS" {
			log = logger.Logger{
				Logger: zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger(),
			}
		}

		if env.myConf.Logparam.Mode == "NATS" {
			// Initial nats connection
			nc, err := ConnectLoopNats(1*time.Minute, env.myConf.Nats.User, env.myConf.Nats.Token, env.myConf.Nats.URL)
			if err != nil {
				// log.Error().Err(err).Msg("Error with NATS")
				panic(fmt.Sprintf("can't write log to nats: %v", err))
			}
			fmt.Println("111")
			nc.Publish("_LOG", []byte("All is Well"))
			log = logger.Logger{
				Logger: zerolog.New(&NatLog{nats: nc}).With().Timestamp().Logger(),
			}
		}

	} else {
		deflogpath := "/var/log/"
		if env.myConf.Logparam.Path != "default" {
			deflogpath = env.myConf.Logparam.Path
		}

		file, err = os.OpenFile(deflogpath+env.myConf.Logparam.File, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			panic(fmt.Sprintf("can't write log file: %v", err))
		}
		// defer file.Close()
		log = logger.Logger{
			Logger: zerolog.New(file).With().Timestamp().Logger(),
		}
	}

	db, err := pgConnect(env.myConf, log)
	if err != nil {
		log.Error().Err(err).Msg("Error with DB")
	}
	log.Info().Str("Initial status", "OK").Time("Time", time.Now()).Msg("System init")

	// // Initial nats connection
	// nc, err := ConnectLoopNats(1*time.Minute, env.myConf.Nats.User, env.myConf.Nats.Token, env.myConf.Nats.URL)
	// if err != nil {
	// 	log.Error().Err(err).Msg("Error with NATS")
	// }

	return &Config{
		DB:   db,
		Log:  log,
		Info: env.myConf.Info,
		file: file,
		nats: nc,
	}
}
