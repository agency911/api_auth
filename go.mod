module bitbucket.org/agency911/Api_Auth

go 1.13

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/crgimenes/goconfig v1.2.1
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.1+incompatible
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.3.0
	github.com/nats-io/nats.go v1.9.1
	github.com/nuveo/log v0.0.0-20190430190217-44d02db6bdf8 // indirect
	github.com/rs/zerolog v1.17.2
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/shopspring/decimal v0.0.0-20200105231215-408a2507e114 // indirect
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17
)
