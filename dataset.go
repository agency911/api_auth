package main

import (
	"io"

	"bitbucket.org/agency911/Api_Auth/logger"
	"github.com/jackc/pgx"
	"github.com/nats-io/nats.go"
)

type (
	// Структура для доступа к БД
	postgres struct {
		Host     string `json:"host" cfg:"host" cfgDefault:"example.com"`
		Port     int    `json:"port" cfg:"port" cfgDefault:"999"`
		Password string `json:"password" cfg:"password" cfgDefault:"123"`
		Username string `json:"username" cfg:"username" cfgDefault:"username"`
		Database string `json:"database" cfg:"database" cfgDefault:"database"`
	}

	// Структура для системного лога
	systemLog struct {
		Level   string `json:"level" cfg:"level"`
		Path    string `json:"path" cfg:"path"`
		File    string `json:"file" cfg:"file"`
		FileRes string `json:"fileres" cfg:"fileres"`
		Mode    string `json:"mode" cfg:"mode"`
	}

	info struct {
		Title string `json:"level" cfg:"level"`
		Port  string `json:"port" cfg:"port"`
	}

	// Структура основной конфигурации
	configMain struct {
		Logparam systemLog `json:"log" cfg:"log"`
		Postgres postgres  `json:"postgres" cfg:"postgres"`
		Info     info      `json:"info" cfg:"info" `
		Nats     natsCfg   `json:"nats" cfg:"nats"`
	}

	natsCfg struct {
		URL   string `json:"url" cfg:"url"`
		Token string `json:"token" cfg:"token"`
		User  string `json:"user" cfg:"user"`
	}

	//Config - конфигурация
	Config struct {
		DB   *pgx.ConnPool
		Log  logger.Logger
		Info info
		file io.WriteCloser
		nats *nats.Conn
	}

	//Env - Структура конфигурации системы
	Env struct {
		myConf *configMain
	}
)
