package main

// New DB migrations:
//go:generate go-bindata -pkg dbrouter -o dbrouter/sql_migrations.go sql/migrations

// General purpose assets
//go:generate go-bindata -pkg bindata -o bindata/bindata.go -prefix _bindata/ _bindata/...

import (
	"context"
	"fmt"

	rt "bitbucket.org/agency911/Api_Auth/APIRouting"

	// "net"
	"net/http"
	"os"
	"os/signal"
	"time"
)

// Close - close log file
func (c *Config) Close() {
	if c.file != nil {
		c.file.Close()
	}
}

func main() {
	conf := initial()
	defer conf.Close()
	conf.Log.Info().Bool("status", true).Str("Port", fmt.Sprintf(":%v", conf.Info.Port)).Msg("Initialization")
	server := &http.Server{Addr: fmt.Sprintf(":%v", conf.Info.Port), Handler: conf.traceLoggingMiddleware(rt.Init())}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			conf.Log.Error().Err(err).Msg("Start")
		}
		conf.Log.Info().Bool("Init", true).Msg("Start")
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Waiting for SIGINT (pkill -2)
	<-stop
	conf.Log.Warn().Str("Action", "shudtown").Msg("API")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		// handle err
		conf.Log.Error().Err(err).Str("Action", "shudtown").Msg("API")
	} else {
		conf.Log.Warn().Str("Action", "shudtown").Bool("Complite", true).Msg("API")
		conf.file.Close()
	}

}

// ReadUserIP - get users IP adress
func ReadUserIP(r *http.Request) string {

	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}

	return IPAddress
}

func (c *Config) traceLoggingMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		c.Log.Info().Str("Method", req.Method).Str("Endpoint", req.RequestURI).Str("IP", ReadUserIP(req)).Msg("Request")
		h.ServeHTTP(w, req)
	})
}
