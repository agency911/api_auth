package main

import (
	"bitbucket.org/agency911/Api_Auth/binpath"
	"bitbucket.org/agency911/Api_Auth/logger"
	"github.com/crgimenes/goconfig"
	_ "github.com/crgimenes/goconfig/json"
	"github.com/jackc/pgx"
	"github.com/rs/zerolog/log"
)

// DB init
func pgConnect(cfg *configMain, log logger.Logger) (*pgx.ConnPool, error) {
	pgConfig := new(pgx.ConnConfig)
	pgConfig.TLSConfig = nil
	connPoolConfig := pgx.ConnPoolConfig{
		ConnConfig: pgx.ConnConfig{
			Host:     cfg.Postgres.Host,
			User:     cfg.Postgres.Username,
			Password: cfg.Postgres.Password,
			Database: cfg.Postgres.Database,
			LogLevel: pgx.LogLevelWarn,
			Logger:   log,
		},
		MaxConnections: 35,
	}
	return pgx.NewConnPool(connPoolConfig)
}

// Config init
func initConfig() (*configMain, error) {
	myconfig := configMain{}
	goconfig.File = "config.json"
	goconfig.Path = binpath.BinaryPath() + "/"
	err := goconfig.Parse(&myconfig)
	if err != nil {
		log.Error().Err(err).Msg("Error with parsing")
		return nil, err
	}
	return &myconfig, nil
}

// func binaryPath() string {
// 	ex, err := os.Executable()
// 	if err == nil && !strings.HasPrefix(ex, "/var/folders/") && !strings.HasPrefix(ex, "/tmp/go-build") {
// 		return path.Dir(ex)
// 	}
// 	_, callerFile, _, _ := runtime.Caller(1)
// 	ex = filepath.Dir(callerFile)
// 	return ex
// }
